import { NgModel } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HumanResourceManagementLayoutComponent } from './shared/human-resource-management-layout/human-resource-management-layout.component';
import { HumanResourceManagementModule } from './human-resource-management/human-resource-management.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [
    AppComponent,
    HumanResourceManagementLayoutComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HumanResourceManagementModule,
    AuthenticationModule,
    ButtonModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
