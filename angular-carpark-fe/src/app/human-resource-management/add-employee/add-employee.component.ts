import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  employeeInformationForm!: FormGroup
  selectedValue!: string;
  selectedDepartment!: Department;
  departments: Department[];
  gender!: string;
  constructor(private formBuilder: FormBuilder) {
    this.departments = [
      { name: 'Department one' },
      { name: 'Department two' },
      { name: 'Department three' },
      { name: 'Department four' },
      { name: 'Department five' },
    ];
  }

  ngOnInit(): void {
    this.employeeInformationForm = this.formBuilder.group({
      fullName: [],
      phoneNumber: [],
      dateOfBirth: [],
      sex: [],
      address: [],
      email: [],
      acoount: [],
      passsword: [],
      department: [],
    })
  }

  createNewEmployee() {

  }

}

interface Department {
  name: string;
}
