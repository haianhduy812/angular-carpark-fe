import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, Routes, RouterModule } from '@angular/router';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', redirectTo: 'view-employee', pathMatch: 'full' },
  { path: 'view-employee', component: ViewEmployeeComponent },
  { path: 'add-new-employee', component: AddEmployeeComponent },
];

@NgModule({
  declarations: [
    ViewEmployeeComponent,
    AddEmployeeComponent,
  ],
  imports: [
    CommonModule,
    RadioButtonModule,
    DropdownModule,
    FormsModule,
    ReactiveFormsModule,
    RadioButtonModule,
    RouterModule.forChild(routes),
  ]
})
export class HumanResourceManagementModule { }
