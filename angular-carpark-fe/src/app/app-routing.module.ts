import { ViewEmployeeComponent } from './human-resource-management/view-employee/view-employee.component';
import { HumanResourceManagementLayoutComponent } from './shared/human-resource-management-layout/human-resource-management-layout.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEmployeeComponent } from './human-resource-management/add-employee/add-employee.component';

const routes: Routes = [
  { path:'', redirectTo: 'authentication', pathMatch:'full'},
  { path:'authentication', loadChildren: () => import('./authentication/authentication.module').then((m) => m.AuthenticationModule)},
  {
    path: 'human-resource-management-layout', component: HumanResourceManagementLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./human-resource-management/human-resource-management.module').then((m) => m.HumanResourceManagementModule),
      },
    ],
  },
  //test link
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
